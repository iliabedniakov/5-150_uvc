﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.3">
  <POU Name="WF_WorkFlow" Id="{0471c53d-397c-4d68-bfb7-a986af38029b}" SpecialFunc="None">
    <Declaration><![CDATA[//Machines workflow or Process FU collector
//- define instances of all FUs
//- call these in the body   
//- see method mConfigComponents which is called in step startupActive and does configure the
//  components behaviour and options within the machine process (ProductionStateMachine)
//-->to follow the program flow please next see FunctionUnits\FU_XXX for FU internal logic 
FUNCTION_BLOCK WF_WorkFlow EXTENDS ZKS_ProcessManager IMPLEMENTS MTL_itfNode

VAR_INPUT
  	I_xUseCStack		: BOOL;
	I_stCStackRef		: REFERENCE TO APM_CStack;
END_VAR

VAR_OUTPUT
END_VAR

VAR
 //xModulCfgDone		: BOOL;
//dwInitStep			: DWORD;

//<LogFileHandler--------------------------------------------------------------------------
		//fbLogFileHandler	: PM_LogFileHandler();
//>LogFileHandler--------------------------------------------------------------------------

//<Lift YZ ------------------------------------------------------------------------------
	fuLiftXy          : BLP_LiftXY(THIS^);
	
	rPositionYLift	AT %Q* : REAL;
	rPositionZLift	AT %Q* : REAL;
//>Lift XY ----------------------------------------------------------------------------

//<Conveyor----------------------------------------------------------------------------
	fuConveyors				: FU_Conveyors(THIS^);
//>Conveyor----------------------------------------------------------------------------
	fuUVBuffer				: FU_UVBuffer();
//<Lift Manager-----------------------------------------------------------------------		
	fuUVLiftManager			: FU_UVLiftStateMachine(THIS^);
//>Lift Manager------------------------------------------------------------------------

//<Curing Machine 
	fuCuringMachine			: FU_CuringMachine(THIS^);
//>Curing Machine 
END_VAR

]]></Declaration>
    <Implementation>
      <ST><![CDATA[mUpdateAppReferences();
SUPER^();

//<LogFileHandler--------------------------------------------------------------------------
	fbLogFileHandler();
//>LogFileHandler--------------------------------------------------------------------------

//<Lift YZ -----------------------------------------------------------------------------
	fuLiftXy();
	rPositionYLift := LREAL_TO_REAL(fuLiftXy.mggHBot.Y.actPos);
	rPositionZLift := LREAL_TO_REAL(fuLiftXy.mggHBot.Z.actPos);
//>Lift YZ -----------------------------------------------------------------------------

//<Conveyor-----------------------------------------------------------------------------
    fuConveyors();
//>Conveyor-----------------------------------------------------------------------------

//<Buffer-------------------------------------------------------------------------------
    fuUVBuffer();
//>Buffer-------------------------------------------------------------------------------
   
//>BufferManager -----------------------------------------------------------------------	
   fuUVLiftManager();
//>BufferManager --------------------------------------------------------------------------	

//<Curing Machine 
	fuCuringMachine();
//>Curing Mchine ]]></ST>
    </Implementation>
    <Folder Name="StartupInit" Id="{f25f6777-ec18-434b-b0eb-513b6e2d6e26}" />
    <Method Name="mConfigComponents" Id="{0b649417-6d96-4859-a507-144343c5dbed}" FolderPath="StartupInit\">
      <Declaration><![CDATA[METHOD mConfigComponents : BOOL

VAR_INST
	tStartWaitMachineData	: TIME;
END_VAR

VAR CONSTANT
	c_dwSetVersion			: DWORD := 0;
	c_dwStartVersionWrite	: DWORD := 1;
	c_dwConfigLogFile		: DWORD := 2;
	c_dwStartLogFile		: DWORD := 3;
	c_dwCfgTeachIn          : DWORD := 4;
	c_dwCfgComponents		: DWORD := 5;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[mConfigComponents := FALSE; //set to TRUE as soon as last call has be done

CASE dwInitStep OF 
	c_dwSetVersion:	
		tStartWaitMachineData 								:= TIME();
				
		xProcessGhostMode 									:= TRUE;	//set to TRUE here, only used as simulation
		dwInitStep := c_dwStartVersionWrite; 
	//>c_dwSetVersion
	
	c_dwStartVersionWrite:
		IF ((gvlAPMMAchine.sMachineName <> '') AND (gvlAPMMAchine.sMachineSerial <> '')) OR (AMC_TimePassedMilliSec(tStartWaitMachineData) > 30000) THEN
			//wait until the machine info is available
			mSetVersionInfo();
			THIS^.mStartVersionFile();
			dwInitStep := c_dwConfigLogFile;
		END_IF
	//>c_dwStartVersionWrite

	c_dwConfigLogFile:
		//common logfile
		fbLogFileHandler.I_sLogfilePath						:= 'C:\TwinCAT\Manz\Log\Debug\';
		fbLogFileHandler.I_sLogFileName 					:= 'Debug';
		fbLogFileHandler.I_sLogFileExt						:= 'log';
		fbLogFileHandler.I_udiMaxBytes 						:= 10485760;		//10MB
		fbLogFileHandler.I_sLogFileSep						:= ';';
		fbLogFileHandler.I_itfProcessManager				:= THIS^;
		dwInitStep := c_dwStartLogFile;
	//>c_dwConfigLogFile

	c_dwStartLogFile:
		IF (fbLogFileHandler.mCfgDataLog()) THEN
			dwInitStep := c_dwCfgTeachIn;
		END_IF
	//>c_dwStartLogFile	
	
	c_dwCfgTeachIn:
        I_rManzTP                                        REF= MAIN.fbMachine.fbTP;    //!!! add cyclic update to mUpdateComponentReferences

        //define names of uo too 3 Main Page Groups
        sTPMainPageGroupTop                         := 'LiftXyUp';
       
        
        //register up to 3 main page groups
        I_rManzTP.regGroup(sTPMainPageGroupTop,     1, 1);
       
        
        //<Organize FU selection on TP - do this for each single FU
        fuLiftXy.I_rManzTP                          REF= MAIN.fbMachine.fbTP;
        fuLiftXy.I_sTPMainPageGroupName             := sTPMainPageGroupTop;    //main group (1st TP Page) up to 3 main groups may be available 
        fuLiftXy.I_usiTPSubPageIndex                := 1;                    //below main group there are up to 3 SubPages (can be changed by wiping)
        fuLiftXy.I_usiTPFctSubPagePos               := 1;                    //each SubPage has 3 positions TopMidBottom 1,2,3
        //>Organize FU selection on TP
            
        dwInitStep := c_dwCfgComponents;
    //>c_dwCfgTeachIn
    //>c_dwCfgTeachIn
		
	c_dwCfgComponents:
		//<Base settings for ProcessManager
		THIS^.sNodeName 							:= 'ProcessManager';
		mResetComponentRegistrations();
       
  //<Organize FU selection on TP - do this for each single FU
        fuLiftXy.I_rManzTP                    REF= MAIN.fbMachine.fbTP;
		
//		I_itfMES 									:= fuMES;	//dummy MES FB in use
//		I_udiMESErrorOffset 						:= 1000;
//		I_xUseMES									:= TRUE;
//		fuMES.I_itfLogFileHandler					:= fbLogFileHandler;
		                                            
		I_uiCycleTimeStatisticsLow					:= 2;	//Time Last n (default n:=1, min=1, max=100)
		I_uiCycleTimeStatisticsMedium				:= 10;	//Time Last n (default n:=10, min=1, max=100)
		I_uiCycleTimeStatisticsHigh					:= 100;	//Time Last n (default n:=100, min=1, max=100)	
		//>Base settings for ProcessManager	
				
//>Conveyor---------------------------------------------------------------------------------------------------------
		fuConveyors.sNodeName					    :='Conveyors';
		fuConveyors.I_xPauseSyncReq				    := TRUE;	//must reply to pause req 
		fuConveyors.I_xCyclicProcess				:= TRUE;	//process run is not cmd controlled
		fuConveyors.PrepareBasePosLevels 		    := MTL_eAutomaticModeLevel.Level3;                               
		fuConveyors.BasePosLevels 				    := MTL_eAutomaticModeLevel.Level3;                               
		fuConveyors.OpeningCycleLevels 			    := MTL_eAutomaticModeLevel.Level3;                               
		fuConveyors.ClosingCycleLevels 			    := MTL_eAutomaticModeLevel.Level3;
		fuConveyors.I_uiStopLevel				    := 3;
		fuConveyors.I_uiPauseLevel				    := 3;
//		fuConveyorBottom.I_itfMES				    		:= fuMES;
		fuConveyors.I_udiMESErrorOffset			    := 8000;
		fuConveyors.I_xUseMES					    := FALSE;
		fuConveyors.itf2ManagerPick				    := fuUVLiftManager.itfPickPos;
		
		fuConveyors.mCfgSubModules();
		
		mRegisterComponent(fuConveyors);     		
//>ConveyorTop-----------------------------------------------------------------------------------------------------
	
//>LiftXY----------------------------------------------------------------------------------------------------------
        fuLiftXy.sNodeName                          := 'LiftXyUp';
		fuLiftXy.I_iNumberOfPlacesInBuffer			:= 8;
		fuLiftXy.I_xPauseSyncReq				    := TRUE;	//must reply to pause req 
		fuLiftXy.I_xCyclicProcess				    := TRUE;	//process run is not cmd controlled
		fuLiftXy.PrepareBasePosLevels 			    := MTL_eAutomaticModeLevel.Level2;                               
		fuLiftXy.BasePosLevels   			        := MTL_eAutomaticModeLevel.Level2;                               
		fuLiftXy.OpeningCycleLevels 		        := MTL_eAutomaticModeLevel.Level2;                               
		fuLiftXy.ClosingCycleLevels 		        := MTL_eAutomaticModeLevel.Level2;
		fuLiftXy.I_uiStopLevel					    := 2;
		fuLiftXy.I_uiPauseLevel				        := 2;
//		fuLiftXyUp.I_itfMES							:= fuMES;
		fuLiftXy.I_udiMESErrorOffset		        := 9000;
		fuLiftXy.I_xUseMES						    := FALSE;
		fuLiftXy.xHWInOutGapSensorInverted          := TRUE;
		fuLiftXy.I_xHasBuffer						:= TRUE;
						
		mRegisterComponent(fuLiftXy);     		
//<LiftXY---------------------------------------------------------------------------------------------------------- 


//<Buffer--------------------------------------------------------------------------------------------------------
		fuUVBuffer.I_sDebugName := 'Buffer Place UV';
        fuUVBuffer.I_xSimulated := FALSE;
        fuUVBuffer.mInit(I_sCfgMainPathDataLogging := 'C:\TwinCat\Manz\PersistentData\',
                       I_sCfgSubPathDataLogging := 'FU_BufferPLaceUV\',
                       I_eProxyTypeAtSlots := eSensorType.NORMALLY_OPEN,
                       I_uiNumberOfPlacesInBuffer := 1,
                       I_sCfgFileNameDataLogging := 'DataLoggingBuffer');
                       
//>Buffer--------------------------------------------------------------------------------------------------------

//<Lift Manager UV
			fuUVliftManager.sNodeName						:='LiftManagerDown';
			fuUVliftManager.I_xPauseSyncReq					:= TRUE;	//must reply to pause req 
			fuUVliftManager.I_xCyclicProcess				:= TRUE;	//process run is not cmd controlled
			fuUVliftManager.PrepareBasePosLevels 			:= MTL_eAutomaticModeLevel.Level1;                               
			fuUVliftManager.BasePosLevels   				:= MTL_eAutomaticModeLevel.Level1;                               
			fuUVliftManager.OpeningCycleLevels 				:= MTL_eAutomaticModeLevel.Level1;                               
			fuUVliftManager.ClosingCycleLevels 				:= MTL_eAutomaticModeLevel.Level1;
			fuUVliftManager.I_uiStopLevel					:= 1;
			fuUVliftManager.I_uiPauseLevel					:= 1;
//			fuUVliftManager.I_itfMES						:= fuMES;
			fuUVliftManager.I_udiMESErrorOffset				:= 10000;
			fuUVliftManager.I_xUseMES						:= FALSE;           
//            fuUVliftManager.I_xProdErrSyncCheckStopByLevel := TRUE;
            fuUVliftManager.itfLift                       	:= fuLiftXy;
			fuUVliftManager.itfPickPos						:= THIS^.fuConveyors.itf2ManagerPick;
            fuUVliftManager.itfPlacePos                   	:= THIS^.fuConveyors.itf2ManagerPlace;
            fuUVliftManager.itfBuffer                     	:= fuUVBuffer;       // = 0 means has not buffer configured 
 
			mRegisterComponent(fuUVliftManager);     		
        
            fuUVliftManager.mConfig(I_sCfgPersistMainPath := 'C:\TwinCat\Manz\PersistentData\',
                                  I_sCfgPersistDataSubPath  := 'FU_UVLiftManager\',
                                  I_sCfgPersistDataFileName := 'LiftManagerDown',
                                  I_sCfgPersistDataFileExt  := 'dat',
                                  I_xUseDataFlow            := TRUE);
//>Lift Manager UV
		
	//>c_dwCfgComponents	
	
	mConfigComponents := TRUE;	
END_CASE
]]></ST>
      </Implementation>
    </Method>
    <Method Name="mSetVersionInfo" Id="{346a7c89-5352-45b1-bee6-f8873e07c49a}" FolderPath="StartupInit\">
      <Declaration><![CDATA[METHOD mSetVersionInfo
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//<Version Info on Scada
	stState.VersionInfo.addApplicationLib('ALU_ApplicationLibUtils', 		ALU_ApplicationLibUtils.stLibVersion_ALU_ApplicationLibUtils.sVersion, '');
	stState.VersionInfo.addApplicationLib('ALM_ApplicationLibMotion',	 	ALM_ApplicationLibMotion.stLibVersion_ALM_ApplicationLibMotion.sVersion, '');
	stState.VersionInfo.addApplicationLib('ALS_ApplicationLibScada', 		ALS_ApplicationLibScada.stLibVersion_ALS_ApplicationLibScada.sVersion, '');
	stState.VersionInfo.addApplicationLib('ALD_ApplicationLibDevices', 		ALD_ApplicationLibDevices.stLibVersion_ALD_ApplicationLibDevices.sVersion, '');

	stState.VersionInfo.setApplicationInfo('PassageWay', 				    stLibVersion_PassageWay.sVersion, gvlAPMMachine.sMachineSerial);
//>Version Info on Scada

//<define needed lib versions for check
	//requested application libs
	mRegAndCheckLibVersion('ALU_ApplicationLibUtils',		115, 0, 0, 5,	ALU_ApplicationLibUtils.stLibVersion_ALU_ApplicationLibUtils, 		I_xMajorMustFit := TRUE, I_xMinorMustFit := TRUE, I_xBuildMustFit := TRUE, I_xRevisionMustFit := TRUE);	//if value is false, the corresponding version may have a higher number 
	mRegAndCheckLibVersion('ALM_ApplicationLibMotion', 		115, 0, 0, 4, 	ALM_ApplicationLibMotion.stLibVersion_ALM_ApplicationLibMotion, 	I_xMajorMustFit := TRUE, I_xMinorMustFit := TRUE, I_xBuildMustFit := TRUE, I_xRevisionMustFit := TRUE);	//if value is false, the corresponding version may have a higher number 
	mRegAndCheckLibVersion('ALS_ApplicationLibScada',		115, 0, 0, 2, 	ALS_ApplicationLibScada.stLibVersion_ALS_ApplicationLibScada,		I_xMajorMustFit := TRUE, I_xMinorMustFit := TRUE, I_xBuildMustFit := TRUE, I_xRevisionMustFit := TRUE);	//if value is false, the corresponding version may have a higher number 
	mRegAndCheckLibVersion('ALD_ApplicationLibDevices',		115, 0, 0, 1,	ALD_ApplicationLibDevices.stLibVersion_ALD_ApplicationLibDevices,	I_xMajorMustFit := TRUE, I_xMinorMustFit := TRUE, I_xBuildMustFit := TRUE, I_xRevisionMustFit := TRUE);	//if value is false, the corresponding version may have a higher number 

	//requested MTL libs
	mRegAndCheckLibVersion('MTL_AutomationBase', 			1, 15, 0, 0,	MTL_AutomationBase.stLibVersion_MTL_AutomationBase,					I_xMajorMustFit := TRUE, I_xMinorMustFit := TRUE, I_xBuildMustFit := TRUE, I_xRevisionMustFit := TRUE);	//if value is false, the corresponding version may have a higher number 
//	mRegAndCheckLibVersion('MTL_Metrology', 				1, 15, 0, 0,	MTL_Metrology.stLibVersion_MTL_Metrology,							I_xMajorMustFit := TRUE, I_xMinorMustFit := TRUE, I_xBuildMustFit := TRUE, I_xRevisionMustFit := TRUE);	//if value is false, the corresponding version may have a higher number 
	mRegAndCheckLibVersion('MTL_MotionAndDrives', 			1, 15, 0, 0,	MTL_MotionAndDrives.stLibVersion_MTL_MotionAndDrives,				I_xMajorMustFit := TRUE, I_xMinorMustFit := TRUE, I_xBuildMustFit := TRUE, I_xRevisionMustFit := TRUE);	//if value is false, the corresponding version may have a higher number 
	mRegAndCheckLibVersion('MTL_OpcUaClient', 				1, 15, 0, 0,	MTL_OpcUaClient.stLibVersion_MTL_OpcUaClient,						I_xMajorMustFit := TRUE, I_xMinorMustFit := TRUE, I_xBuildMustFit := TRUE, I_xRevisionMustFit := TRUE);	//if value is false, the corresponding version may have a higher number
	mRegAndCheckLibVersion('MTL_Utils', 					1, 15, 0, 0,	MTL_Utils.stLibVersion_MTL_Utils,									I_xMajorMustFit := TRUE, I_xMinorMustFit := TRUE, I_xBuildMustFit := TRUE, I_xRevisionMustFit := TRUE);	//if value is false, the corresponding version may have a higher number 
	
	mRegAddVersion('Scada'			, '2.2.0.277'	, 'n.a.');	
	mRegAddVersion('Drive-FW'		, '17V20'		, 'n.a.');
	mRegAddVersion('OPC/UA Server'	, '3.3.10.0'	, 'n.a.');
	mRegAddVersion('TCP/IP Server'	, '3.1.3.0'		, 'n.a.');
	
//<define needed lib versions for check
]]></ST>
      </Implementation>
    </Method>
    <Method Name="mUpdateAppReferences" Id="{0f479ee0-496d-4de7-9784-798ce675e86a}" FolderPath="StartupInit\">
      <Declaration><![CDATA[METHOD mUpdateAppReferences
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[//all used references must be updated each cycle to enable a 
//a safe Online-Change

//<points,motion-------------------------------------------------
	I_rManzTP						REF= MAIN.fbMachine.fbTP;
	fuLiftXy.I_rManzTP	        REF= MAIN.fbMachine.fbTP;
//>points,motion-------------------------------------------------
]]></ST>
      </Implementation>
    </Method>
    <Method Name="startupActive" Id="{8e34eda6-8466-4c68-953c-0b8ef69af6e2}" FolderPath="StartupInit\">
      <Declaration><![CDATA[// user function called from MTL when startup mode is active, can be overwritten from application
// startup mode will be left when stState.conditions.xStartupFinish is set to TRUE
// MUST NOT BE CALLED FROM APPLICATION
{attribute 'object_name' := 'startupActive'}
METHOD startupActive : BOOL
]]></Declaration>
      <Implementation>
        <ST><![CDATA[IF (NOT xModulCfgDone) THEN
	xModulCfgDone := THIS^.mConfigComponents();
ELSE
	fbLogFileHandler.mWriteData('startup completed', '', '', '');
	THIS^.stState.conditions.xStartupFinish := TRUE;
END_IF
]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="WF_WorkFlow">
      <LineId Id="503" Count="7" />
      <LineId Id="662" Count="0" />
      <LineId Id="512" Count="2" />
      <LineId Id="519" Count="7" />
      <LineId Id="569" Count="0" />
      <LineId Id="615" Count="0" />
      <LineId Id="2" Count="0" />
      <LineId Id="616" Count="0" />
      <LineId Id="708" Count="0" />
      <LineId Id="707" Count="0" />
      <LineId Id="710" Count="0" />
      <LineId Id="709" Count="0" />
    </LineIds>
    <LineIds Name="WF_WorkFlow.mConfigComponents">
      <LineId Id="3" Count="23" />
      <LineId Id="28" Count="10" />
      <LineId Id="383" Count="0" />
      <LineId Id="385" Count="2" />
      <LineId Id="390" Count="2" />
      <LineId Id="394" Count="3" />
      <LineId Id="399" Count="5" />
      <LineId Id="421" Count="3" />
      <LineId Id="384" Count="0" />
      <LineId Id="39" Count="1" />
      <LineId Id="42" Count="2" />
      <LineId Id="381" Count="1" />
      <LineId Id="371" Count="0" />
      <LineId Id="48" Count="3" />
      <LineId Id="53" Count="4" />
      <LineId Id="337" Count="1" />
      <LineId Id="157" Count="12" />
      <LineId Id="639" Count="0" />
      <LineId Id="341" Count="0" />
      <LineId Id="170" Count="0" />
      <LineId Id="342" Count="0" />
      <LineId Id="171" Count="2" />
      <LineId Id="175" Count="1" />
      <LineId Id="479" Count="0" />
      <LineId Id="177" Count="10" />
      <LineId Id="494" Count="0" />
      <LineId Id="523" Count="0" />
      <LineId Id="495" Count="0" />
      <LineId Id="189" Count="1" />
      <LineId Id="576" Count="0" />
      <LineId Id="454" Count="1" />
      <LineId Id="615" Count="6" />
      <LineId Id="453" Count="0" />
      <LineId Id="480" Count="0" />
      <LineId Id="642" Count="0" />
      <LineId Id="650" Count="12" />
      <LineId Id="664" Count="0" />
      <LineId Id="666" Count="0" />
      <LineId Id="678" Count="0" />
      <LineId Id="668" Count="9" />
      <LineId Id="643" Count="0" />
      <LineId Id="575" Count="0" />
      <LineId Id="321" Count="0" />
      <LineId Id="537" Count="0" />
      <LineId Id="355" Count="0" />
      <LineId Id="322" Count="0" />
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="WF_WorkFlow.mSetVersionInfo">
      <LineId Id="3" Count="28" />
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="WF_WorkFlow.mUpdateAppReferences">
      <LineId Id="3" Count="5" />
      <LineId Id="10" Count="0" />
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="WF_WorkFlow.startupActive">
      <LineId Id="3" Count="5" />
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>