@echo off
setlocal enabledelayedexpansion
set B=%~dp0
pushd ..
set CWD=%cd%
popd
@echo off

rem echo %CWD%
if /I "%CWD%" NEQ "%MSDK_CONFIGURATIONS%" goto error
set B=!B:%MSDK_CONFIGURATIONS%\=!
set B=!B:\=!
echo %B%> "%MSDK_CONFIGURATIONS%\ConfigLink.txt"
set SOURCE=%~dp0\VisiWin\Project\ManzScada.runtime.vwsettings
set DESTINATION=%MSDK_BIN_ROOT%\%MSDK_VERSION%\bin\%MSDK_PLATFORM_CONFIG%\SCADA\ManzScada.runtime.vwsettings
copy "%SOURCE%" "%DESTINATION%" >nul
del "%MSDK_CONFIGURATIONS%\ReplacerInfo.xml" 2>nul

echo.
echo Set AutostartEnabled=0 for all found projects...
for /f "tokens=* usebackq" %%G in (`reg query "HKLM\SOFTWARE\Wow6432Node\INOSOFT GmbH\VisiWin\7.2\Projects" /v AutostartEnabled /s ^| find "Projects"`) do (
	rem echo %%G 
	reg add "%%G" /v AutostartEnabled /t REG_DWORD /d 0x0 /f  >nul)

echo.
echo Register current project and set AutostartEnabled=1...
reg add "HKLM\SOFTWARE\Wow6432Node\INOSOFT GmbH\VisiWin\7.2\Projects\%B%.ManzScada_Server" /v Path /t REG_SZ /d "%~dp0VisiWin\Project\Server" /f >nul
reg add "HKLM\SOFTWARE\Wow6432Node\INOSOFT GmbH\VisiWin\7.2\Projects\%B%.ManzScada_Server" /v VisiWinNETVersion /t REG_SZ /d "7.2" /f  >nul
reg add "HKLM\SOFTWARE\Wow6432Node\INOSOFT GmbH\VisiWin\7.2\Projects\%B%.ManzScada_Server" /v ShutdownMode /t REG_SZ /d "RunUntilClientsLeft" /f  >nul
reg add "HKLM\SOFTWARE\Wow6432Node\INOSOFT GmbH\VisiWin\7.2\Projects\%B%.ManzScada_Server" /v ProjectType /t REG_SZ /d "Server" /f  >nul
reg add "HKLM\SOFTWARE\Wow6432Node\INOSOFT GmbH\VisiWin\7.2\Projects\%B%.ManzScada_Server" /v AutostartEnabled /t REG_DWORD /d 0x1 /f  >nul

echo Create version info for "Add/Remove Programs"
reg add "HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\ScadaActiveConfig" /v DisplayVersion /t REG_SZ /d "unknown" /f  >nul
FOR /F "delims=" %%G IN (version.txt) DO (
	reg add "HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\ScadaActiveConfig" /v DisplayVersion /t REG_SZ /d "%%G" /f  >nul
	)
reg add "HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\ScadaActiveConfig" /v DisplayIcon /t REG_EXPAND_SZ /d "C:\Program Files (x86)\Manz AG\ManzSDK\%%MSDK_VERSION%%\bin\SCADA\ManzSCADA.exe,-32512" /f  >nul
reg add "HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\ScadaActiveConfig" /v DisplayName /t REG_SZ /d " SCADA Active Config: %B%" /f  >nul
reg add "HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\ScadaActiveConfig" /v Publisher /t REG_SZ /d "Manz AG" /f  >nul
reg add "HKLM\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\ScadaActiveConfig" /v UninstallString /t REG_SZ /d "dummy" /f  >nul

exit /B

:error
@echo off
echo ***********************************************************
echo Selected configuration is not located in configurations 
echo directory 
echo '%MSDK_CONFIGURATIONS%'.
echo Move configuration or change configurations location.
echo.
echo Selected configuration has not been activated.
echo ***********************************************************
pause
exit /B

